# Aldi Rhiyadi

## 🚀 About Me
A final year Informatics and Computer Engineering Education student with a passion for Cloud Computing.

As a student at Semarang State University, I have gained a solid foundation in computer science principles and programming concepts. I have also gained teaching skills that support me in communicating and being a good leader.

However, my genuine ardor lies within the realm of cloud computing, with a special emphasis on deployment practices. I derive immense satisfaction from the journey of transforming an application or service from its developmental stage to a fully operational state. This process of transitioning to production fuels my enthusiasm, and I am ceaselessly engaged in the quest for novel approaches to enhance and optimize the deployment procedures.

To that end, I have explored Google Cloud Platform (GCP) as my primary cloud computing platform. I have also worked on several cloud-based projects, both independently and as part of a team.

## Let's Connect
- Linkedin : https://www.linkedin.com/in/aldi-rhiyadi-59464b25b/
- github : https://github.com/Wazven